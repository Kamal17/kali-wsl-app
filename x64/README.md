# rootfs (AMD64)

Put Kali-WSL rootfs (AMD64) here: `install.tar.gz`

URL: https://gitlab.com/kalilinux/build-scripts/kali-wsl-rootfs

Path: `.\x64\install.tar.gz`
