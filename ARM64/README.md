# rootfs (ARM64)

Put Kali-WSL rootfs (ARM64) here: `install.tar.gz`

URL: https://gitlab.com/kalilinux/build-scripts/kali-wsl-rootfs

Path: `.\ARM64\install.tar.gz`
